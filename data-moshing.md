

<http://bitsynthesis.com/2009/04/tutorial-datamoshing-the-beauty-of-glitch/>

**how do we use our knowledge of I- and P-frames to datamosh?**
* remove all the I-frames from Clip B
	* insert P-frames from Clip B after an I-frame, 
	* or a series of I- and P-frames, from Clip A 

the movement from Clip B, as described in its P-frames, is translated to the image from Clip A. 
Clip B's P-frames act on the pixels in Clip A according to their original instructions.

```
Examples:
(clip A) I - P - P - P - P - P - (clip B) I - P - P - P - P - P - P
(clip A) I - P - B - P - B - P - (clip B) I - P - B - P - B - P - B
```

```
Example:
(clip A) I - P - P - P - P - P - P - (clip B) P - P - P - P - P - P - P
```


Another way to get some crazy effects is to:

* copy the same P-frame over and over again in sequence. 
This results in color drifts and smears as the same pixel moves and manipulations are repeated over and over on top of themselves. 

```
Example:
I - P1 - P2 - P3 - P3 - P3 - P3 - P3 - P3 - P3 - P3 - P3 - P3 - P3 - P3
```


If you want to clear the mess of datamoshed nonsense partway through your clip, just insert a new I-frame. Since I-frames are effectively still images, this will snap the image back to whatever is described in that I-frame.

## binary data manipulation
According to <http://rosa-menkman.blogspot.com/2009/02/how-to-datamoshing-create-compression.html>

In in a avi file (xvid codec)

    An I-Frame can be identified by the HEX string 00 01 B0 01 that will appear about 5 bytes after the 00dc frame start marker.


Knowing that, what needs to be done is to remove i-frame bytes from xvid (coded) videos.
By readind a binary video (xvid), searching for the I-frame hex string, and removing that string + a few more bytes. The result is a damaged I-frame header. Which is what we intended

see av




## FFMPEG

**How can I copy the same P-frame, over and over again?**
* by saving all the frames as images, manipulating thos images, for instances copying the same P-frame over and over, and rebuilkding the video from it
8 using ffmpeg filter. but how? 


*I tried doing the copying of repeated P-frames, but the result is not what I expected. 
It is a slowed down video, where it gets stuck in another frame* 


**in making all frames into images, I am producing actual frames (I-frames), where previously were only


# discarding frames
ffmpeg allows discarding frames to the input 

``` 

-discard (input)

    Allows discarding specific streams or frames from streams. Any input stream can be fully discarded, using value all whereas selective discarding of frames from a stream occurs at the demuxer and is not supported by all demuxers.

    none

        Discard no frame.
    default

        Default, which discards no frames.
    noref

        Discard all non-reference frames.
    bidir

        Discard all bidirectional frames.
    nokey

        Discard all frames excepts keyframes.
    all

        Discard all frames. 

```
`ffmpeg -discard bidir -discard noref -i test.mp4   -vf select="eq(pict_type\,P)"  -an -framerate 25 -vcodec libxvid -f avi test_no_bidir.avi`