#!/usr/bin/env python3
import os
import subprocess
import shlex
import curses
from multiprocessing import Process
from time import sleep


class Videomoshing:
    """docstring for Videomoshing"""
    def __init__(self, recfile, noiframesfile):
        # function returns the path and files that will be used
        self.path = os.path.dirname(os.path.abspath(__file__))
        self.recfile = "{}/{}".format(self.path, recfile)
        self.recfiletmp = "{}/.{}".format(self.path, recfile)
        self.noiframesfile = "{}/{}".format(self.path, noiframesfile)
        # purge files
        for f in [self.recfile, self.recfiletmp, self.noiframesfile]:
            if os.path.isfile(f):
                os.remove(f)

    def rec_loop(self, rec_dur):
        while True:
            rec_camera_cmd = "ffmpeg -y -f v4l2 -t {} \
                -framerate 25 -video_size 1280x720\
                -i /dev/video0 -vcodec libxvid \
                {}".format(rec_dur, self.recfiletmp)
            rec_camera_cmd = shlex.split(rec_camera_cmd)
            subprocess.call(rec_camera_cmd,
                            stdout=subprocess.DEVNULL,
                            stderr=subprocess.DEVNULL)
            # print("recording finished")
            os.rename(self.recfiletmp, self.recfile)

    def nuke_iframes(self, skipframes=2):
        while True:
            # loop until rec_file exists
            if os.path.isfile(self.recfile):
                with open(self.recfile, "r+b") as avi:
                    hex_dump = avi.read()
                while True:
                    # find frame start marker b'\x00\xdc'
                    found = hex_dump.find(b'\x00\x01\xb0')
                    if found > -1:
                        # print("found I frame",
                        #       found,
                        #       hex_dump[found:found + 2])
                        # remove from start marker to the to 5bytes later
                        hex_dump = hex_dump[:found] + \
                            hex_dump[(found + skipframes):]
                    else:
                        break
                with open(self.noiframesfile, "w+b") as avi:
                    avi.write(hex_dump)
                os.remove(self.recfile)

    def playback_loop(self):
        while True:
            playback_cmd = "mpv -geometry=640x360+1+1 {}".format(self.noiframesfile)
            # note geometry position in not working
            playback_cmd = shlex.split(playback_cmd)
            subprocess.call(playback_cmd,
                            stdout=subprocess.DEVNULL,
                            stderr=subprocess.DEVNULL)


def main(stdscr):
    """checking for keypress"""
    stdscr.nodelay(True)  # do not wait for input when calling getch
    return stdscr.getch()


def killmpv():
    # kill last created instance of mpv
    mpv_processes = subprocess.check_output(shlex.split("pgrep -f mpv"))
    mpv_processes = (mpv_processes.decode('ascii')).split('\n')
    mpv_processes = mpv_processes[:-1]  # ignore last list item: empty string
    subprocess.call(shlex.split("kill {}".format(mpv_processes[-1])))


if __name__ == '__main__':
    try:
        v = Videomoshing(recfile="rec.avi", noiframesfile="rec_noiframes.avi")
        print(v.path, v.recfile, v.noiframesfile)
        # args: duration of video
        process_rec = Process(target=v.rec_loop, args=(10,))
        process_rec.start()
        # args: bytes removed
        process_iframes = Process(target=v.nuke_iframes, args=(20,))
        process_iframes.start()
        process_playback = []
        process_playback.append(Process(target=v.playback_loop))
        process_playback[-1].start()
        # keyboard listening loop
        while True:
            key = curses.wrapper(main)
            if key is 10:  # return/enter key: create playback
                process_playback.append(Process(target=v.playback_loop))
                process_playback[-1].start()
            elif key is 32:  # space key: stop playback
                if len(process_playback) > 0:
                    process_playback[-1].terminate()
                    process_playback.pop(-1)
                    killmpv()
            sleep(0.01)

    except KeyboardInterrupt:
        process_rec.terminate()
        process_iframes.terminate()
        for process in process_playback:
            process.terminate()  # do it for all



