# Video Moshing loop

## videomoshing.py 
`class Videomoshing` includes

Three methods, each creatings its own loop, run as parallel processes

* `rec_loop(rec_dur)` - captures the webcam and encodes it into the file `rec.avi`
* `nuke_iframes(skipframes=)` - search for `rec.avi`, reads it and removes its I-frame bytes, saving it as  `rec_noiframes.avi`

* `playback_loop` uses mpv to playback `rec_noiframes.avi` 


### keyboard interaction
Using return key to stop/start playback